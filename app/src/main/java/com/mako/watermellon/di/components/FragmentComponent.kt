package com.mako.watermellon.di.components

import com.mako.watermellon.di.modules.FragmentModule
import dagger.Subcomponent
import com.mako.watermellon.di.PerFragment
import com.mako.watermellon.presentation.router.RouterInterface
import com.mako.watermellon.presentation.ui.fragments.authorization.login.LoginFragment
import com.mako.watermellon.presentation.ui.fragments.authorization.registration.RegistrationFragment


@PerFragment
@Subcomponent(modules = [FragmentModule::class])
interface FragmentComponent {
    fun inject(fragment: LoginFragment)
    fun inject(fragment: RegistrationFragment)

    fun router(): RouterInterface

}