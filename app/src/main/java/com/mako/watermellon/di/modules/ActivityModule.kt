package com.mako.watermellon.di.modules

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.mako.watermellon.di.ActivityContext
import com.mako.watermellon.presentation.router.Router
import com.mako.watermellon.presentation.router.RouterInterface
import dagger.Module
import dagger.Provides


@Module
class ActivityModule(private val mActivity: AppCompatActivity) {

    @Provides
    fun provideActivity(): Activity = mActivity





    @Provides
    @ActivityContext
    fun providesContext(): Context = mActivity


    @Provides
    fun provideRouter(): RouterInterface = Router(mActivity)
}