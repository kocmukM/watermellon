package com.mako.watermellon.di.modules

import android.app.Service
import android.content.Context
import dagger.Module
import dagger.Provides
import com.mako.watermellon.di.ServiceContext


@Module
class ServiceModule(private val service: Service) {

    @Provides
    internal fun provideService(): Service = service

    @Provides
    @ServiceContext
    internal fun providesContext(): Context = service

}