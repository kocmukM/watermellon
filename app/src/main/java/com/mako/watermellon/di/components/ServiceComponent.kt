package com.mako.watermellon.di.components

import com.mako.watermellon.di.modules.ServiceModule
import dagger.Component
import com.mako.watermellon.di.PerService


@PerService
@Component(dependencies = [ApplicationComponent::class], modules = [ServiceModule::class])
interface ServiceComponent {

}