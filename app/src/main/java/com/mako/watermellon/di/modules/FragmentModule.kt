package com.mako.watermellon.di.modules

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.mako.watermellon.presentation.router.Router
import com.mako.watermellon.presentation.router.RouterInterface
import dagger.Module
import dagger.Provides


@Module
class FragmentModule(private val fragment: Fragment) {

    private val activity: AppCompatActivity = fragment.activity!! as AppCompatActivity

    @Provides
    internal fun provideActivity(): Activity = activity

    @Provides
    internal fun provideFragment(): Fragment = fragment

    @Provides
    fun provideRouter(): RouterInterface = Router(activity)
}