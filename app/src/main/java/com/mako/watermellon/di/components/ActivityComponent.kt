package com.mako.watermellon.di.components

import com.mako.watermellon.di.modules.ActivityModule
import dagger.Subcomponent
import com.mako.watermellon.di.PerActivity
import com.mako.watermellon.presentation.router.RouterInterface
import com.mako.watermellon.presentation.ui.activities.authorization.AuthorizationActivity


@PerActivity
@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {
    fun inject(activity: AuthorizationActivity)

    fun router(): RouterInterface
}