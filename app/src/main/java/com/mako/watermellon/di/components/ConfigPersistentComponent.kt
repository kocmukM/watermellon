package com.mako.watermellon.di.components

import com.mako.watermellon.di.modules.ActivityModule
import com.mako.watermellon.di.modules.FragmentModule
import dagger.Component
import com.mako.watermellon.di.ConfigPersistent


@ConfigPersistent
@Component(dependencies = [ApplicationComponent::class])
interface ConfigPersistentComponent {
    fun activityComponent(activityModule: ActivityModule): ActivityComponent
    fun fragmentComponent(fragmentModule: FragmentModule): FragmentComponent
    fun serviceComponent(serviceComponent: ServiceComponent): ServiceComponent
}