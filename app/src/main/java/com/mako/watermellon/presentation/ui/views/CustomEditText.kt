package com.mako.watermellon.presentation.ui.views

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.GradientDrawable
import android.text.Editable
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.annotation.ColorRes
import com.mako.watermellon.R
import kotlinx.android.synthetic.main.view_edit_text.view.*


class CustomEditText(context: Context, attrs: AttributeSet) : RelativeLayout(context, attrs) {
    init {
        LayoutInflater.from(context).inflate(R.layout.view_edit_text, this)
        applyAttributes(attrs)
    }

    @ColorRes
    private var textColor: ColorStateList? = null
    @ColorRes
    private var defColor: ColorStateList? = null
    @ColorRes
    private var errorColor: ColorStateList? = null

    private var hint: String? = null

    //    private var maxWidth: Int? = null
    private var hintSize: Float? = null

    private var inputType: InputType? = InputType.TEXT

    private var actionIsActive = false

    private fun applyAttributes(attrs: AttributeSet) {
        context.theme.obtainStyledAttributes(attrs, R.styleable.CustomEditText, 0, 0).apply {

            hint = getString(R.styleable.CustomEditText_hint)
            custom_hint.text = hint


            textColor = getColorStateList(R.styleable.CustomEditText_textColor)
                    ?: ColorStateList.valueOf(resources.getColor(R.color.colorAccent))

            custom_text.setTextColor(textColor)




            defColor = getColorStateList(R.styleable.CustomEditText_defColor)
                    ?: ColorStateList.valueOf(resources.getColor(R.color.colorPrimary))

            setColor(defColor!!)



            errorColor = getColorStateList(R.styleable.CustomEditText_errorColor)
                    ?: ColorStateList.valueOf(resources.getColor(R.color.colorError))
            custom_error.setTextColor(errorColor)

//            maxWidth = getInteger(R.styleable.CustomEditText_maxWidth, 0)
            hintSize = getDimension(R.styleable.CustomEditText_hintSize, 14f)
            setHintSize(hintSize!!)

            if (hasValue(R.styleable.CustomEditText_inputType)) {
                when (getInt(R.styleable.CustomEditText_inputType, 0)) {
                    0 -> {
                        inputType = InputType.TEXT
                        setInputType(InputType.TEXT)
                    }
                    1 -> {
                        inputType = InputType.PASSWORD
                        setInputType(InputType.PASSWORD)
                    }

                    2 -> {
                        inputType = InputType.PHONE
                        setInputType(InputType.PHONE)
                    }
                }
            }

//            val layoutParams = this@CustomEditText.layoutParams
//            if(maxWidth!! > 0 && layoutParams.width > maxWidth!!)
//                layoutParams.width = maxWidth!!
//
//            this@CustomEditText.layoutParams = layoutParams

            recycle()
        }
    }


    fun setInputType(inputType: InputType) {
        when (inputType) {
            InputType.TEXT -> {
                custom_text.inputType = android.text.InputType.TYPE_CLASS_TEXT
            }
            InputType.PASSWORD -> {
                setAsPassword()
            }
            InputType.PHONE -> {
                setAsPhoneNumber()
            }
        }
    }

    fun setAsPassword() {
        actionIsActive = false
        custom_text.transformationMethod = PasswordTransformationMethod.getInstance()
        custom_action.setBackgroundResource(R.drawable.password_eye)
        custom_action.setOnClickListener {
            if (!actionIsActive) {
                custom_text.inputType = android.text.InputType.TYPE_CLASS_TEXT
            } else {
                custom_text.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            actionIsActive = !actionIsActive
        }

    }

    fun setAsPhoneNumber() {
        custom_text.inputType = android.text.InputType.TYPE_CLASS_NUMBER or android.text.InputType.TYPE_NUMBER_FLAG_DECIMAL
        val placeholder = "+? (???) ??? - ?? - ??"
        val symbolArray = placeholder.toCharArray()

        custom_text.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
//                val digits = custom_text.getText().toString().length
//                if (digits > 1)
//                    lastChar = custom_text.getText().toString().substring(digits - 1)
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val position = custom_text.length() - 1
                if (before < count) {
                    if (position == symbolArray.size) {
                        custom_text.setText(custom_text.text.substring(0, custom_text.length() - 1))
                        return
                    }
                    if (symbolArray[position] != '?') {
                        var nextPosition = position
                        val sequenceToAdd = StringBuilder()
                        while (symbolArray[nextPosition] != '?') {
                            sequenceToAdd.append(symbolArray[nextPosition])
                            nextPosition++
                        }
                        sequenceToAdd.append(custom_text.text.substring(custom_text.length() - 1, custom_text.length()))
                        custom_text.setText(custom_text.text.substring(0, custom_text.length() - 1) + sequenceToAdd.toString())
                    }
                } else {
                    if (position == -1)
                        return
                    if (symbolArray[position] != '?') {
                        custom_text.setText(custom_text.text.substring(0, custom_text.length() - 1))
                    }
                }
                custom_text.setSelection(custom_text.text.length)
            }

            override fun afterTextChanged(s: Editable) {

            }
        })
    }

    private fun clearSpecificPhoneSymbol(position: Int) {
        if (position == 0) {
            custom_text.setText("")
        }
        if (position == 3 || position == 8) {
            custom_text.setText(custom_text.text.substring(0, custom_text.length() - 2))
        }
        if (position == 14 || position == 19) {
            custom_text.setText(custom_text.text.substring(0, custom_text.length() - 3))
        }
    }


    fun setHintSize(size: Float) {
        custom_hint.textSize = size
    }

    fun setText(text: String) {
        custom_text.setText(text)
    }

    fun getText(): String {
        return if (inputType == InputType.PHONE) {
            custom_text.text.toString().replace("+", "").replace(" ", "").replace("(", "").replace(")", "").replace("-", "")
        } else {
            custom_text.text.toString()
        }
    }

    fun setHint(hint: String) {
        custom_hint.setText(hint)
    }

    fun setError(errorText: String) {
        setColor(errorColor!!)
        custom_error.text = errorText
        custom_error_container.visibility = View.VISIBLE

    }

    fun clearError() {
        setColor(defColor!!)
        custom_error_container.visibility = View.GONE
    }

    private fun setColor(color: ColorStateList) {
        (border.background as GradientDrawable).setStroke(4, color)
        custom_hint.setTextColor(color)
        custom_error.setTextColor(color)
    }

    enum class InputType {
        TEXT,
        PASSWORD,
        PHONE
    }
}