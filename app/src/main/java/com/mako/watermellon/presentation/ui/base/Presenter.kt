package com.mako.watermellon.presentation.ui.base


interface Presenter<in V : ViewContract> {
    fun attachView(view: V)
    fun detachView()

}