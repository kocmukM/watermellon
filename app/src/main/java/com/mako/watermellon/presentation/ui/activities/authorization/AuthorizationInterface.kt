package com.mako.watermellon.presentation.ui.activities.authorization

import com.mako.watermellon.presentation.ui.base.Presenter
import com.mako.watermellon.presentation.ui.base.ViewContract

interface AuthorizationInterface {
    interface AuthorizationView: ViewContract {

    }

    interface AuthorizationPresenter: Presenter<AuthorizationView> {
        fun onViewCreated()
    }
}