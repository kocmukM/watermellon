package com.mako.watermellon.presentation.ui.base


interface ViewContract : ViewContractErrors, ViewContractProgress, ViewContractKeyboard

interface ViewContractErrors {
    fun showError(exception: Exception)
}

interface ViewContractKeyboard {
    fun hideKeyboard()
}

interface ViewContractProgress {

}