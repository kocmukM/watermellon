package com.mako.watermellon.presentation.ui.views

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.view.animation.Animation
import android.view.animation.Transformation


class ExpandableContainer(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {
    private var expandedHeight = /*layoutParams.height*/ 200

    fun expand(milliseconds: Int): Animation {
        return ResizeAnimation(0, expandedHeight, milliseconds)
    }

    fun collapse(milliseconds: Int): Animation {
        return ResizeAnimation(expandedHeight, 0, milliseconds)
    }

    fun makeExpanded() {
        layoutParams.height = expandedHeight
    }

    fun makeCollapsed() {
        layoutParams.height = 0
    }

    private inner class ResizeAnimation(private val mFromHeight: Int, private val mToHeight: Int, durationMillis: Int) : Animation() {

        init {
            duration = durationMillis.toLong()
        }

        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            val height = (mToHeight - mFromHeight) * interpolatedTime + mFromHeight
            layoutParams.height = height.toInt()
            requestLayout()
        }
    }
}