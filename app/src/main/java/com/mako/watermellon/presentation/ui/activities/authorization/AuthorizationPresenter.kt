package com.mako.watermellon.presentation.ui.activities.authorization

import com.mako.watermellon.presentation.router.RouterInterface
import com.mako.watermellon.presentation.ui.base.BasePresenter
import javax.inject.Inject

class AuthorizationPresenter @Inject constructor(private val router: RouterInterface) : BasePresenter<AuthorizationInterface.AuthorizationView>(),
        AuthorizationInterface.AuthorizationPresenter {

    override fun onViewCreated() {
        router.showRegistrationFragment()
    }

}