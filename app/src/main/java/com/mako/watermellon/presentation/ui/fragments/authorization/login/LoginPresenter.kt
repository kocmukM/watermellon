package com.mako.watermellon.presentation.ui.fragments.authorization.login

import com.mako.watermellon.presentation.router.RouterInterface
import com.mako.watermellon.presentation.ui.base.BasePresenter
import javax.inject.Inject

class LoginPresenter @Inject constructor(private val router: RouterInterface): BasePresenter<LoginInterface.LoginView>(), LoginInterface.LoginPresenter{
    override fun registerClicked() {
        router.showRegistrationFragment()
    }

}