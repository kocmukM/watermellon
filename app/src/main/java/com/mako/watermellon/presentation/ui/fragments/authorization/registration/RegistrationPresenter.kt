package com.mako.watermellon.presentation.ui.fragments.authorization.registration

import com.mako.watermellon.presentation.router.RouterInterface
import com.mako.watermellon.presentation.ui.base.BasePresenter
import javax.inject.Inject

class RegistrationPresenter @Inject constructor(private val router: RouterInterface): BasePresenter<RegistrationInterface.RegistrationView>(), RegistrationInterface.RegistrationPresenter {
    override fun loginClicked() {
        router.showLoginFragment()
    }
}