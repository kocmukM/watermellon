package com.mako.watermellon.presentation.ui.fragments.authorization.registration

import android.os.Bundle
import android.view.*
import com.mako.watermellon.R
import com.mako.watermellon.presentation.ui.activities.authorization.AuthorizationActivity
import com.mako.watermellon.presentation.ui.base.BaseFragment
import javax.inject.Inject

class RegistrationFragment : BaseFragment(), RegistrationInterface.RegistrationView {
    companion object {
        fun newInstance(): RegistrationFragment {
            return RegistrationFragment()
        }
    }


    @Inject
    lateinit var presenter: RegistrationPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)
        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AuthorizationActivity).setToolbarText(resources.getString(R.string.fragment_register_title))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if(menu.hasVisibleItems())
            menu.clear()
        inflater.inflate(R.menu.registration_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.login -> {
                presenter.loginClicked()
            }
        }
        return true
    }
}