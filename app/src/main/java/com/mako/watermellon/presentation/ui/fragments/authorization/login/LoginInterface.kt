package com.mako.watermellon.presentation.ui.fragments.authorization.login

import com.mako.watermellon.presentation.ui.base.Presenter
import com.mako.watermellon.presentation.ui.base.ViewContract

interface LoginInterface {
    interface LoginView: ViewContract {

    }

    interface LoginPresenter: Presenter<LoginView> {
        fun registerClicked()
    }
}