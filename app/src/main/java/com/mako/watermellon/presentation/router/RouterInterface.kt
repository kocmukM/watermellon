package com.mako.watermellon.presentation.router

interface RouterInterface {
    fun showLoginFragment()
    fun showRegistrationFragment()
}