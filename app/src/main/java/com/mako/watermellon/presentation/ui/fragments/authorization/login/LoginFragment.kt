package com.mako.watermellon.presentation.ui.fragments.authorization.login

import android.os.Bundle
import android.view.*
import com.mako.watermellon.R
import com.mako.watermellon.presentation.ui.activities.authorization.AuthorizationActivity
import com.mako.watermellon.presentation.ui.base.BaseFragment
import javax.inject.Inject


class LoginFragment : BaseFragment(), LoginInterface.LoginView {
    companion object {
        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }

    @Inject
    lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentComponent.inject(this)
        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if(menu.hasVisibleItems())
            menu.clear()
        inflater.inflate(R.menu.login_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.register -> {
                presenter.registerClicked()
            }
        }
        return true
    }
}