package com.mako.watermellon.presentation.ui.activities.authorization

import android.os.Bundle
import com.mako.watermellon.R
import com.mako.watermellon.presentation.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_authorization.*
import javax.inject.Inject

class AuthorizationActivity : BaseActivity(), AuthorizationInterface.AuthorizationView {
    @Inject
    lateinit var presenter: AuthorizationPresenter

    override fun hideKeyboard() {

    }

    override fun showError(exception: Exception) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent.inject(this)
        presenter.attachView(this)
        setContentView(R.layout.activity_authorization)
        presenter.onViewCreated()
    }

    fun setToolbarText(text: String) {
        title = text
    }
}
