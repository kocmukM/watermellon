package com.mako.watermellon.presentation.router

import androidx.appcompat.app.AppCompatActivity
import com.mako.watermellon.R
import com.mako.watermellon.presentation.ui.fragments.authorization.login.LoginFragment
import com.mako.watermellon.presentation.ui.fragments.authorization.registration.RegistrationFragment

class Router(private val activity: AppCompatActivity): RouterInterface {
    override fun showLoginFragment() {
        val fragment = LoginFragment.newInstance()
        activity.supportFragmentManager.beginTransaction().add(R.id.authorization_container, fragment).commit()

    }

    override fun showRegistrationFragment() {
        val fragment = RegistrationFragment.newInstance()
        activity.supportFragmentManager.beginTransaction().add(R.id.authorization_container, fragment).commit()
    }
}