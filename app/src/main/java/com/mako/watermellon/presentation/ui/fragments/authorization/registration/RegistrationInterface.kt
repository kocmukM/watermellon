package com.mako.watermellon.presentation.ui.fragments.authorization.registration

import com.mako.watermellon.presentation.ui.base.Presenter
import com.mako.watermellon.presentation.ui.base.ViewContract

interface RegistrationInterface {
    interface RegistrationView: ViewContract {

    }

    interface RegistrationPresenter: Presenter<RegistrationView> {
        fun loginClicked()
    }
}