package com.mako.watermellon.presentation.ui.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.mako.watermellon.WatermellonApplication
import com.mako.watermellon.di.components.DaggerConfigPersistentComponent
import com.mako.watermellon.di.components.FragmentComponent
import com.mako.watermellon.di.modules.FragmentModule
import com.mako.watermellon.presentation.ui.activities.authorization.AuthorizationActivity


abstract class BaseFragment : Fragment(), ViewContract {

    lateinit var fragmentComponent: FragmentComponent
        private set

    lateinit var activityContext: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val configPersistentComponent = DaggerConfigPersistentComponent.builder()
                    .applicationComponent(WatermellonApplication.getComponent())
                    .build()
        fragmentComponent = configPersistentComponent.fragmentComponent(FragmentModule(this))
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activityContext = context!!
        (context as BaseActivity).attachFragment(this)
    }

//    abstract fun onParentActivityResult(requestCode: Int, resultCode: Int, data: Intent?)

//    abstract fun onBackPressed()

    override fun showError(exception: Exception) {
        (activity!! as BaseActivity).showError(exception)
    }

    override fun hideKeyboard() {
        val imm = activityContext.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity!!.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}