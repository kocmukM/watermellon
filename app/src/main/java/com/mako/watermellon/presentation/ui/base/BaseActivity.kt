package com.mako.watermellon.presentation.ui.base

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mako.watermellon.WatermellonApplication
import com.mako.watermellon.di.components.ActivityComponent
import com.mako.watermellon.di.components.DaggerConfigPersistentComponent
import com.mako.watermellon.di.modules.ActivityModule

abstract class BaseActivity : AppCompatActivity(), ViewContract {
    lateinit var baseFragment: BaseFragment
    lateinit var activityComponent: ActivityComponent
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val configPersistentComponent = DaggerConfigPersistentComponent.builder()
                .applicationComponent(WatermellonApplication.getComponent())
                .build()
        activityComponent = configPersistentComponent.activityComponent(ActivityModule(this))

    }

    fun attachFragment(baseFragment: BaseFragment) {
        this.baseFragment = baseFragment
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        baseFragment.onParentActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
//        baseFragment.onBackPressed()
    }

    abstract override fun showError(exception: Exception)
}