package com.mako.watermellon

import android.app.Application
import com.mako.watermellon.di.components.ApplicationComponent
import com.mako.watermellon.di.components.DaggerApplicationComponent
import com.mako.watermellon.di.modules.ApplicationModule

class WatermellonApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        private var mApplicationComponent: ApplicationComponent? = null
        var instance:WatermellonApplication? = null

        fun getComponent(): ApplicationComponent {
            if (mApplicationComponent == null) {
                mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(ApplicationModule(instance!!))
                    .build()
            }
            return mApplicationComponent as ApplicationComponent
        }
    }
}