package com.mako.watermellon.data.network

import kotlinx.coroutines.Deferred
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitApiService {
    @GET("hello/world")
    fun getHelloWorld(): Deferred<Response<ResponseBody>>
}