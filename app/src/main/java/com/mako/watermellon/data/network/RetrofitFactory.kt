package com.mako.watermellon.data.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.mako.watermellon.BuildConfig
import retrofit2.Retrofit


object RetrofitFactory {
    fun buildRetrofitApiService(): RetrofitApiService {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build().create(RetrofitApiService::class.java)

    }
}